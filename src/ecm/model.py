import logging
from collections import defaultdict
from typing import Dict, List, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sbtab
from equilibrator_api import Q_, ComponentContribution
from sbtab import SBtab
from scipy.io import savemat

from ecm.colors import ColorMap
from ecm.cost_function import EnzymeCostFunction
from ecm.errors import ThermodynamicallyInfeasibleError
from ecm.html_writer import BaseHtmlWriter
from ecm.kegg_model import KeggModel, KeggReaction, ParseReaction
from ecm.util import (
    ECF_DEFAULTS,
    RT,
    PlotCorrelation,
    QuantitiesToColumnVector,
    str2bool,
)


class ECMmodel(object):

    DATAFRAME_NAMES = [
        "Compound",
        "Reaction",
        "ConcentrationConstraint",
        "Parameter",
        "RelativeFlux",
    ]

    def __init__(self, df_dict: pd.DataFrame, ecf_params: dict = None):

        for n in ECMmodel.DATAFRAME_NAMES:
            if n not in df_dict.keys():
                raise KeyError(
                    "A DataFrame named %s is missing from the model" % n
                )

        self.ecf_params = dict(ECF_DEFAULTS)
        if ecf_params is not None:
            self.ecf_params.update(ecf_params)

        comp_df = (
            df_dict["Compound"].copy().set_index("Identifiers:kegg.compound")
        )
        if "NameForPlots" in comp_df:
            self.kegg2met = comp_df["NameForPlots"]
        elif "Name" in comp_df:
            self.kegg2met = comp_df["Name"]
        else:
            raise KeyError(
                'Column "Name" or "NameForPlots" must be given for the Compounds table'
            )

        # a dictionary indicating which compound is external or not
        if "External" in comp_df:
            self.cid2external = comp_df["External"].apply(str2bool)
        elif "IsConstant" in comp_df:
            self.cid2external = comp_df["IsConstant"].apply(str2bool)
        else:
            raise KeyError(
                'Column "External" or "IsConstant" must be given for the Compounds table'
            )

        rxn_df = df_dict["Reaction"].copy().set_index("ID")
        if "NameForPlots" in rxn_df:
            self.kegg2rxn = rxn_df["NameForPlots"]
        elif "Name" in rxn_df:
            self.kegg2rxn = rxn_df["Name"]
        else:
            raise KeyError(
                'Column "Name" or "NameForPlots" must be given for the Reactions table'
            )

        self.kegg_model = ECMmodel.GenerateKeggModel(
            df_dict["Compound"], df_dict["Reaction"]
        )

        self.cid2min_bound, self.cid2max_bound = self.ReadConcentrationBounds(
            df_dict["ConcentrationConstraint"]
        )

        (
            rid2crc_gmean,
            rid2crc_fwd,
            rid2crc_rev,
            rid_cid2KMM,
            rid2keq,
            rid2mw,
            cid2mw,
        ) = ECMmodel.ReadParameters(df_dict["Parameter"])

        if self.ecf_params["dG0_source"] == "keq_table":
            self._CalcGibbsEnergiesFromKeq(rid2keq)
        elif self.ecf_params["dG0_source"] == "component_contribution":
            self._CalcGibbsEnerigesFromComponentContribution()
        elif self.ecf_params["dG0_source"] == "dG0r_table":
            self._CalcGibbsEnerigesFromdG0ReactionTable()
        else:
            raise ValueError(
                "unrecognized dG0 source: " + self.ecf_params["dG0_source"]
            )

        flux = (
            df_dict["RelativeFlux"]
            .set_index("Reaction")
            .loc[self.kegg_model.rids, "Value"]
            .apply(float)
        )
        flux = np.array(flux.values, ndmin=2, dtype=float).T

        if self.ecf_params["flux_unit"]:
            # convert fluxes to M/s if they are in some other absolute unit
            r = Q_(1.0, self.ecf_params["flux_unit"]).to("M/s").magnitude
            flux *= r

        # we only need to define get kcat in the direction of the flux
        # if we use the 'gmean' option, that means we assume we only know
        # the geometric mean of the kcat, and we distribute it between
        # kcat_fwd and kcat_bwd according to the Haldane relationship
        # if we use the 'fwd' option, we just take the kcat in the
        # direction of flux (as is) and that would mean that our
        # thermodynamic rate law would be equivalent to calculating the
        # reverse kcat using the Haldane relationship
        if self.ecf_params["kcat_source"] == "gmean":
            kcat = QuantitiesToColumnVector(
                map(rid2crc_gmean.get, self.kegg_model.rids), "1/s"
            )
        elif self.ecf_params["kcat_source"] == "fwd":
            # get the relevant kcat (fwd/rev) depending on the direction of flux
            kcat = []
            for r, rid in enumerate(self.kegg_model.rids):
                if flux[r] >= 0:
                    kcat.append(rid2crc_fwd[rid])
                else:
                    kcat.append(rid2crc_rev[rid])
            kcat = QuantitiesToColumnVector(kcat, "1/s")
        else:
            raise ValueError(
                "unrecognized kcat source: " + self.ecf_params["kcat_source"]
            )

        dG0 = QuantitiesToColumnVector(
            map(self.rid2dG0.get, self.kegg_model.rids), "kJ/mol"
        )

        KMM = ECMmodel._GenerateKMM(
            self.kegg_model.cids, self.kegg_model.rids, rid_cid2KMM
        )

        lbs = QuantitiesToColumnVector(
            map(self.cid2min_bound.get, self.kegg_model.cids), "M"
        )
        ubs = QuantitiesToColumnVector(
            map(self.cid2max_bound.get, self.kegg_model.cids), "M"
        )
        lnC_bounds = np.log(np.hstack([lbs, ubs]))

        # we need all fluxes to be positive, so for every negative flux,
        # we multiply it and the corresponding column in S by (-1)
        dir_mat = np.diag(np.sign(flux + 1e-10).flat)
        flux = dir_mat @ flux
        S = self.kegg_model.S @ dir_mat
        dG0 = dir_mat @ dG0

        mw_enz = QuantitiesToColumnVector(
            map(rid2mw.get, self.kegg_model.rids), "Da"
        )

        mw_met = QuantitiesToColumnVector(
            map(cid2mw.get, self.kegg_model.cids), "Da"
        )

        self.ecf = EnzymeCostFunction(
            S,
            flux=flux,
            kcat=kcat,
            dG0=dG0,
            KMM=KMM,
            mw_enz=mw_enz,
            mw_met=mw_met,
            lnC_bounds=lnC_bounds,
            params=self.ecf_params,
        )

        self._val_df_dict = None

    @staticmethod
    def FromSBtab(sbtabdoc: sbtab.SBtab.SBtabDocument, ecf_params: dict = None):
        if ecf_params is None:
            ecf_params = dict()

        bound_sbtab = sbtabdoc.get_sbtab_by_id("ConcentrationConstraint")
        ecf_params["bound_unit"] = bound_sbtab.get_attribute("Unit")

        flux_sbtab = sbtabdoc.get_sbtab_by_id("RelativeFlux")
        if flux_sbtab is None:
            raise ValueError(
                'The input SBtab does not have a table named "RelativeFlux"'
            )
        ecf_params["flux_unit"] = flux_sbtab.get_attribute("Unit")

        df_dict = {
            sbtab.table_id: sbtab.to_data_frame() for sbtab in sbtabdoc.sbtabs
        }

        return ECMmodel(df_dict, ecf_params=ecf_params)

    def AddValidationData(self, validate_sbtabdoc: sbtab.SBtab.SBtabDocument):
        conc_sbtab = validate_sbtabdoc.get_sbtab_by_id("Concentration")
        self._met_conc_unit = Q_(conc_sbtab.get_attribute("Unit"))
        assert self._met_conc_unit.check("[concentration]"), (
            f"Metabolite concentration unit is not a [concentration] quantity",
            self._met_conc_unit,
        )

        enzyme_sbtab = validate_sbtabdoc.get_sbtab_by_id("EnzymeConcentration")
        self._enz_conc_unit = Q_(enzyme_sbtab.get_attribute("Unit"))
        assert self._enz_conc_unit.check("[concentration]"), (
            f"Enzyme concentration unit is not a [concentration] quantity",
            self._enz_conc_unit,
        )

        self._val_df_dict = {
            sbtab.table_id: sbtab.to_data_frame()
            for sbtab in validate_sbtabdoc.sbtabs
        }

    def WriteMatFile(self, file_name: str):
        mdict = self.ecf.Serialize()
        mdict["cids"] = self.kegg_model.cids
        mdict["rids"] = self.kegg_model.rids
        with open(file_name, "wb") as fp:
            savemat(fp, mdict, format="5")

    @staticmethod
    def GenerateKeggModel(compound_df: pd.DataFrame, reaction_df: pd.DataFrame):
        # first, parse the reaction formulas and create a dictionary
        # for each one (i.e. "sparse reaction")
        sparse_reactions = reaction_df["ReactionFormula"].apply(ParseReaction)

        # use the compound table to convert the IDs to KEGG IDs
        met2kegg = compound_df.set_index("ID")["Identifiers:kegg.compound"]
        map_met2kegg = lambda spr: {met2kegg[k]: v for (k, v) in spr.items()}
        try:
            sparse_reactions_kegg = list(map(map_met2kegg, sparse_reactions))
        except KeyError as e:
            raise KeyError(
                "One of the metabolites in the reaction formulas "
                "is not defined in the Compound table: " + str(e)
            )

        kegg_reactions = [
            KeggReaction(s, rid=rid)
            for (s, rid) in zip(sparse_reactions_kegg, reaction_df["ID"])
        ]

        model = KeggModel.from_kegg_reactions(
            kegg_reactions, has_reaction_ids=True
        )
        return model

    @staticmethod
    def ReadParameters(parameter_df: pd.DataFrame) -> Tuple[dict, ...]:
        cols = [
            "QuantityType",
            "Value",
            "Compound:Identifiers:kegg.compound",
            "Reaction",
            "Unit",
        ]

        rid2mw = defaultdict(float)
        cid2mw = defaultdict(float)
        rid2keq = {}
        rid2crc_gmean = {}  # catalytic rate constant geomertic mean
        rid2crc_fwd = {}  # catalytic rate constant forward
        rid2crc_rev = {}  # catalytic rate constant reverse
        crctype2dict = {
            "catalytic rate constant geometric mean": rid2crc_gmean,
            "substrate catalytic rate constant": rid2crc_fwd,
            "product catalytic rate constant": rid2crc_rev,
        }

        rid_cid2KMM = {}  # Michaelis-Menten constants

        for i, row in parameter_df.iterrows():
            try:
                typ, val, cid, rid, unit = [row[c] for c in cols]
                val = Q_(float(val), unit)

                if typ in crctype2dict:
                    assert val.check("1/[time]")
                    val.ito("1/s")
                    crctype2dict[typ][rid] = val
                elif typ == "Michaelis constant":
                    assert val.check("[concentration]")
                    val.ito("molar")
                    rid_cid2KMM[rid, cid] = val
                elif typ == "equilibrium constant":
                    assert val.check("")
                    rid2keq[rid] = val
                elif typ == "protein molecular mass":
                    assert val.check("[mass]")
                    val.ito("Da")
                    rid2mw[rid] = val
                elif typ == "molecular mass":
                    assert val.check("[mass]")
                    val.ito("Da")
                    cid2mw[cid] = val
                else:
                    raise AssertionError(
                        "unrecognized Rate Constant Type: " + typ
                    )
            except AssertionError:
                raise ValueError(
                    "Syntax error in Parameter table, row %d - %s" % (i, row)
                )
        # make sure not to count water as contributing to the volume or
        # cost of a reaction
        return (
            rid2crc_gmean,
            rid2crc_fwd,
            rid2crc_rev,
            rid_cid2KMM,
            rid2keq,
            rid2mw,
            cid2mw,
        )

    def _GibbsEnergyFromMilliMolarToMolar(self, dGm: np.array) -> np.array:
        """
            In the current SBtab file format, the 'standard Gibbs energies'
            are actually dGm (since mM is used as the reference concentration).
            We need to convert them back to M and that requires using the
            stoichiometric matrix (self.kegg_model.S).
        """

        # Assume that all concentrations are 1 mM
        ln_conc = np.log(1e-3) * np.ones((self.kegg_model.S.shape[0], 1))

        # subtract the effect of the concentrations to return back to dG0
        dG0 = dGm - self.kegg_model.S.T @ ln_conc * RT

        return dG0

    def _CalcGibbsEnergiesFromKeq(self, rid2keq: Dict[str, float]) -> None:
        keq = QuantitiesToColumnVector(
            map(rid2keq.get, self.kegg_model.rids), ""
        )
        dGm_prime = -RT * np.log(keq)
        dG0_prime = self._GibbsEnergyFromMilliMolarToMolar(dGm_prime)

        self.rid2dG0 = dict(zip(self.kegg_model.rids, dG0_prime.flat))

    def _CalcGibbsEnerigesFromComponentContribution(self) -> None:
        # convert the kegg_model to a "real" KeggModel, i.e. one that
        # supports component-contributions
        cc = ComponentContribution(
            p_h=Q_(7.5),
            ionic_strength=Q_(0.1, "M"),
            temperature=Q_(298.15, "K"),
        )

        dG0_prime, sqrt_Sigma = cc.standard_dg_prime_multi(self.model.reactions)

        self.rid2dG0 = dict(zip(self.kegg_model.rids, dG0_prime.flat))

    def _CalcGibbsEnerigesFromdG0ReactionTable(self) -> None:
        """
            it's better to take the values from the SBtab since they are processed
            by the parameter balancing funcion
        """
        # TODO: Add test for this function

        gibbs_sbtab = self._model_sbtab.get_sbtab_by_id("GibbsEnergyOfReaction")

        dGm_prime = (
            gibbs_sbtab.to_data_frame()
            .set_index("Reaction")
            .loc[self.kegg_model.rids, "Value"]
            .apply(float)
        )
        dGm_prime = np.array(dGm_prime.values, ndmin=2, dtype=float).T

        dG0_units = Q_(gibbs_sbtab.get_attribute("Unit"))
        assert dG0_units.check("[energy]/[substance]")
        dGm_prime *= dG0_units

        dG0_prime = self._GibbsEnergyFromMilliMolarToMolar(dGm_prime)

        self.rid2dG0 = dict(zip(self.kegg_model.rids, dG0_prime.flat))

    def ReadConcentrationBounds(
        self, bound_df: pd.DataFrame
    ) -> Tuple[Dict[str, float], Dict[str, float]]:
        """
            read the lower and upper bounds and convert them to M.
            verify that the values make sense.
        """
        unit = Q_(self.ecf_params["bound_unit"])

        cid2min_bound = {
            row["Compound:Identifiers:kegg.compound"]: float(
                row["Concentration:Min"]
            )
            * unit
            for _, row in bound_df.iterrows()
        }
        cid2max_bound = {
            row["Compound:Identifiers:kegg.compound"]: float(
                row["Concentration:Max"]
            )
            * unit
            for _, row in bound_df.iterrows()
        }

        for cid in cid2min_bound.keys():
            assert cid2min_bound[cid] <= cid2max_bound[cid]
        return cid2min_bound, cid2max_bound

    @staticmethod
    def _GenerateKMM(
        cids: List[str], rids: List[str], rid_cid2KMM: dict
    ) -> np.array:
        KMM = np.ones((len(cids), len(rids)))
        for i, cid in enumerate(cids):
            for j, rid in enumerate(rids):
                kmm = rid_cid2KMM.get((rid, cid), Q_("M"))
                KMM[i, j] = kmm.to("M").magnitude
        return KMM

    def MDF(self):
        mdf, lnC0 = self.ecf.MDF()

        if np.isnan(mdf) or mdf < 0.0:
            logging.error(
                "Negative MDF value: %.1f RT (= %.1f kJ/mol)" % (mdf, mdf * RT)
            )
            raise ThermodynamicallyInfeasibleError()
        return lnC0

    def ECM(self, lnC0=None, n_iter=10):
        if lnC0 is None:
            lnC0 = self.MDF()
            logging.info("initializing ECM using MDF result")

        return self.ecf.ECM(lnC0, n_iter=n_iter)

    def ECF(self, lnC):
        return self.ecf.ECF(lnC)

    @staticmethod
    def _nanfloat(x):
        if type(x) == float:
            return x
        if type(x) == int:
            return float(x)
        if type(x) == str:
            if x.lower() in ["", "nan"]:
                return np.nan
            else:
                return float(x)
        else:
            raise ValueError("unrecognized type for value: " + str(type(x)))

    @staticmethod
    def _MappingToCanonicalEnergyUnits(unit):
        """
            Assuming the canonical units for concentration are Molar

            Returns:
                A function that converts a single number or string to the
                canonical units
        """
        if unit == "kJ/mol":
            return lambda x: ECMmodel._nanfloat(x)
        if unit == "kcal/mol":
            return lambda x: ECMmodel._nanfloat(x) * 4.184

        raise ValueError("Cannot convert these units to kJ/mol: " + unit)

    def _GetMeasuredMetaboliteConcentrations(self):
        if self._val_df_dict is None:
            raise Exception(
                "cannot validate results because no validation data"
                " was given"
            )

        # assume concentrations are in mM
        met_conc_df = self._val_df_dict["Concentration"].set_index(
            "Compound:Identifiers:kegg.compound"
        )
        kegg2conc = met_conc_df["Value"].apply(
            lambda x: Q_(float(x if x else 0), self._met_conc_unit)
        )
        return QuantitiesToColumnVector(kegg2conc[self.kegg_model.cids], "M")

    def _GetMeasuredEnzymeConcentrations(self):
        if self._val_df_dict is None:
            raise Exception(
                "cannot validate results because no validation data"
                " was given"
            )

        enz_conc_df = self._val_df_dict["EnzymeConcentration"].set_index(
            "Reaction"
        )
        rxn2conc = enz_conc_df["Value"].apply(
            lambda x: Q_(float(x if x else 0), self._enz_conc_unit)
        )
        return QuantitiesToColumnVector(rxn2conc[self.kegg_model.rids], "M")

    def _GetVolumeDataForPlotting(self, lnC):
        enz_vols, met_vols = self.ecf.GetVolumes(lnC)

        enz_labels = list(map(self.kegg2rxn.get, self.kegg_model.rids))
        enz_data = sorted(zip(enz_vols.flat, enz_labels), reverse=True)
        enz_vols, enz_labels = zip(*enz_data)
        enz_colors = [(0.5, 0.8, 0.3)] * len(enz_vols)

        met_labels = list(map(self.kegg2met.get, self.kegg_model.cids))
        met_data = zip(met_vols.flat, met_labels)
        # remove H2O from the list and sort by descending volume
        met_data = sorted(filter(lambda x: x[1] != "h2o", met_data))
        met_vols, met_labels = zip(*met_data)
        met_colors = [(0.3, 0.5, 0.8)] * len(met_vols)

        return (
            enz_vols + met_vols,
            enz_labels + met_labels,
            enz_colors + met_colors,
        )

    def PlotVolumes(self, lnC: np.array, ax: plt.axes) -> None:
        width = 0.8
        vols, labels, colors = self._GetVolumeDataForPlotting(lnC)

        ax.bar(np.arange(len(vols)), vols, width, color=colors)
        ax.set_xticklabels(labels, size="medium", rotation=90)
        ax.set_ylabel("total weight [g/L]")

    def PlotVolumesPie(self, lnC: np.array, ax: plt.axes) -> None:
        vols, labels, colors = self._GetVolumeDataForPlotting(lnC)
        ax.pie(vols, labels=labels, colors=colors)
        ax.set_title("total weight [g/L]")

    def PlotThermodynamicProfile(self, lnC: np.array, ax: plt.axes) -> None:
        """
            Plot a cumulative line plot of the dG' values given the solution
            for the metabolite levels. This was originally designed for showing
            MDF results, but is also a useful tool for ECM.
        """
        driving_forces = self.ecf._DrivingForce(lnC)

        dgs = [0] + list((-driving_forces).flat)
        cumulative_dgs = np.cumsum(dgs)

        xticks = np.arange(0, len(cumulative_dgs)) - 0.5
        xticklabels = [""] + list(map(self.kegg2rxn.get, self.kegg_model.rids))
        ax.plot(cumulative_dgs)
        ax.set_xticks(xticks)
        ax.set_xticklabels(xticklabels, rotation=45, ha="right")
        ax.set_xlim(0, len(cumulative_dgs) - 1)
        ax.set_xlabel("")
        ax.set_ylabel("Cumulative $\Delta_r G'$ (kJ/mol)", family="sans-serif")

    def PlotEnzymeDemandBreakdown(
        self,
        lnC: np.array,
        ax: plt.Axes,
        top_level: int = 3,
        plot_measured: bool = False,
    ) -> None:
        """
            A bar plot in log-scale showing the partitioning of cost between
            the levels of kinetic costs:
            1 - capacity
            2 - thermodynamics
            3 - saturation
            4 - allosteric
        """
        assert top_level in range(1, 5)

        costs = np.array(self.ecf.GetEnzymeCostPartitions(lnC))

        # give all reactions with zero cost a base value, which we will
        # also set as the bottom ylim, which will simulate a "minus infinity"
        # when we plot it in log-scale
        base = min(filter(None, costs[:, 0])) / 2.0
        idx_zero = costs[:, 0] == 0
        costs[idx_zero, 0] = base
        costs[idx_zero, 1:] = 1.0

        bottoms = np.hstack(
            [np.ones((costs.shape[0], 1)) * base, np.cumprod(costs, 1)]
        )
        steps = np.diff(bottoms)

        labels = EnzymeCostFunction.ECF_LEVEL_NAMES[0:top_level]

        ind = range(costs.shape[0])  # the x locations for the groups
        width = 0.8
        ax.set_yscale("log")

        if plot_measured:
            all_labels = ["measured"] + labels
            meas_conc = self._GetMeasuredEnzymeConcentrations()
            cmap = ColorMap(
                all_labels,
                saturation=0.7,
                value=1.0,
                hues=[30.0 / 255, 170.0 / 255, 200.0 / 255, 5.0 / 255],
            )
            ax.plot(
                ind,
                meas_conc,
                color=cmap["measured"],
                marker="o",
                markersize=5,
                linewidth=0,
                markeredgewidth=0.3,
                markeredgecolor=(0.3, 0.3, 0.3),
            )
        else:
            all_labels = labels
            cmap = ColorMap(
                labels,
                saturation=0.7,
                value=0.8,
                hues=[170.0 / 255, 200.0 / 255, 5.0 / 255],
            )

        for i, label in enumerate(labels):
            ax.bar(
                ind,
                steps[:, i].flat,
                width,
                bottom=bottoms[:, i].flat,
                color=cmap[label],
            )

        ax.set_xticks(ind)
        xticks = list(map(self.kegg2rxn.get, self.kegg_model.rids))
        ax.set_xticklabels(xticks, size="medium", rotation=90)
        ax.legend(all_labels, loc="best", framealpha=0.2)
        ax.set_ylabel("enzyme demand [M]")
        ax.set_ylim(bottom=base)

    def ValidateMetaboliteConcentrations(
        self, lnC: np.array, ax: plt.Axes, scale: str = "log"
    ) -> None:
        pred_conc = np.exp(lnC)

        meas_conc = self._GetMeasuredMetaboliteConcentrations()

        # remove NaNs and zeros
        mask = np.nan_to_num(meas_conc) > 0
        mask &= np.nan_to_num(pred_conc) > 0

        # remove compounds with fixed concentrations
        mask &= np.diff(self.ecf.lnC_bounds) > 1e-9

        labels = list(map(self.kegg2met.get, self.kegg_model.cids))
        PlotCorrelation(ax, meas_conc, pred_conc, labels, mask, scale=scale)
        ax.set_xlabel("measured [M]")
        ax.set_ylabel("predicted [M]")

    def ValidateEnzymeConcentrations(
        self, lnC: np.array, ax: plt.Axes, scale: str = "log"
    ) -> None:
        pred_conc = self.ecf.ECF(lnC)
        meas_conc = self._GetMeasuredEnzymeConcentrations()

        labels = list(map(self.kegg2rxn.get, self.kegg_model.rids))
        PlotCorrelation(ax, meas_conc, pred_conc, labels, scale=scale)

        ax.set_xlabel("measured [M]")
        ax.set_ylabel("predicted [M]")

    def ToSBtab(self, lnC: np.array) -> sbtab.SBtab.SBtabDocument:
        met_data = []
        for i, cid in enumerate(self.kegg_model.cids):
            met_name = self.kegg2met[cid]
            met_data.append(("concentration", met_name, cid, np.exp(lnC[i, 0])))
        met_df = pd.DataFrame(
            columns=[
                "QuantityType",
                "Compound",
                "Compound:Identifiers:kegg.compound",
                "ecm",
            ],
            data=met_data,
        )

        enz_conc = self.ecf.ECF(lnC)
        enz_data = []
        for i, rid in enumerate(self.kegg_model.rids):
            rxn_name = self.kegg2rxn[rid]
            enz_data.append(
                ("concentration of enzyme", rxn_name, rid, enz_conc[i, 0])
            )
        enz_df = pd.DataFrame(
            columns=[
                "QuantityType",
                "Reaction",
                "Reaction:Identifiers:kegg.reaction",
                "ecm",
            ],
            data=enz_data,
        )

        sbtabdoc = SBtab.SBtabDocument("report")
        met_sbtab = SBtab.SBtabTable.from_data_frame(
            met_df,
            table_id="Predicted concentrations",
            table_type="Quantity",
            unit="M",
        )

        enz_sbtab = SBtab.SBtabTable.from_data_frame(
            enz_df,
            table_id="Predicted enzyme levels",
            table_type="Quantity",
            unit="M",
        )

        sbtabdoc.add_sbtab(met_sbtab)
        sbtabdoc.add_sbtab(enz_sbtab)
        return sbtabdoc

    def WriteHtmlTables(self, lnC: np.array, html: BaseHtmlWriter) -> None:
        meas_enz2conc = self._GetMeasuredEnzymeConcentrations()
        meas_conc = np.array(
            list(
                map(
                    lambda r: meas_enz2conc.get(r, np.nan), self.kegg_model.rids
                )
            ),
            ndmin=2,
        ).T
        data_mat = np.hstack(
            [
                self.ecf.flux,
                meas_conc,
                self.ecf.ECF(lnC),
                self.ecf._DrivingForce(lnC),
                self.ecf.GetEnzymeCostPartitions(lnC),
            ]
        )

        data_mat[:, 0] *= 1e3  # convert flux from M/s to mM/s
        data_mat[:, 1] *= 1e6  # convert measured enzyme conc. from M to uM
        data_mat[:, 2] *= 1e6  # convert predicted enzyme conc. from M to uM
        data_mat[:, 4] *= 1e6  # convert capacity term from M to uM

        headers = [
            "reaction",
            "KEGG ID",
            "flux [mM/s]",
            "measured enz. conc. [uM]",
            "predicted enz. conc. [uM]",
            "driving force [kJ/mol]",
            "capacity [uM]",
        ] + EnzymeCostFunction.ECF_LEVEL_NAMES[1:]
        values = zip(
            map(self.kegg2rxn.get, self.kegg_model.rids),
            self.kegg_model.rids,
            *data_mat.T.tolist(),
        )
        values.append(
            [
                "total",
                "",
                "",
                data_mat[:, 1].sum(),
                data_mat[:, 2].sum(),
                "",
                "",
                "",
                "",
                "",
            ]
        )

        rowdicst = [dict(zip(headers, v)) for v in values]
        html.write_table(rowdicst, headers=headers, decimal=3)

        meas_met2conc = self._GetMeasuredMetaboliteConcentrations()
        meas_conc = np.array(
            list(
                map(
                    lambda r: meas_met2conc.get(r, np.nan), self.kegg_model.cids
                )
            ),
            ndmin=2,
        ).T
        data_mat = np.hstack(
            [meas_conc, np.exp(lnC), np.exp(self.ecf.lnC_bounds)]
        )

        data_mat *= 1e3  # convert all concentrations from M to mM

        headers = [
            "compound",
            "KEGG ID",
            "measured conc. [mM]",
            "predicted conc. [mM]",
            "lower bound [mM]",
            "upper bound [mM]",
        ]
        values = zip(
            map(self.kegg2met.get, self.kegg_model.cids),
            self.kegg_model.cids,
            *data_mat.T.tolist(),
        )
        rowdicst = [dict(zip(headers, v)) for v in values]
        headers = [
            "compound",
            "KEGG ID",
            "measured conc. [mM]",
            "lower bound [mM]",
            "predicted conc. [mM]",
            "upper bound [mM]",
        ]
        html.write_table(rowdicst, headers=headers)
