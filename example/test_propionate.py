import inspect
import logging
import os

import matplotlib.pyplot as plt
from sbtab import SBtab

from ecm.model import ECMmodel


l = logging.getLogger()
l.setLevel(logging.INFO)

SCRIPT_DIR = os.path.dirname(
    os.path.abspath(inspect.getfile(inspect.currentframe()))
)
DATA_DIR = os.path.join(os.path.split(SCRIPT_DIR)[0], "data")
RESULT_DIR = os.path.join(os.path.split(SCRIPT_DIR)[0], "res")

modeldata_fname = os.path.join(
    DATA_DIR, "propionate_oxidation(CoAT)_pH7.00_I0.10_ECM.tsv"
)

#%%
logging.info("Reading SBtab files")
modeldata_sbtabs = SBtab.read_csv(modeldata_fname, "model")

logging.info("Creating an ECM model using the data")
# ecf_params = {'regularization': None}
ecf_params = {"regularization": "volume"}

model = ECMmodel.FromSBtab(modeldata_sbtabs, ecf_params=ecf_params)

logging.info("Solving MDF problem")
lnC_MDF = model.MDF()
logging.info("Solving ECM problem")
lnC_ECM = model.ECM(n_iter=5)

#%%
fig1 = plt.figure(figsize=(14, 5))
ax_MDF = fig1.add_subplot(1, 2, 1)
model.PlotEnzymeDemandBreakdown(lnC_MDF, ax_MDF, plot_measured=True)
ax_ECM = fig1.add_subplot(1, 2, 2, sharey=ax_MDF)
model.PlotEnzymeDemandBreakdown(lnC_ECM, ax_ECM, plot_measured=True)
fig1.savefig(os.path.join(RESULT_DIR, "demand.svg"))

#%%
fig5 = plt.figure(figsize=(5, 5))
ax = fig5.add_subplot(1, 1, 1)
# model.PlotVolumesPie(lnC_ECM, ax)
vols, labels, colors = model._GetVolumeDataForPlotting(lnC_ECM)
ax.pie(vols, labels=labels, colors=colors)
ax.set_title("total weight = %.2g [g/L]" % sum(vols))

fig5.savefig(os.path.join(RESULT_DIR, "pie.svg"))
