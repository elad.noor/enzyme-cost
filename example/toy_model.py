# -*- coding: utf-8 -*-
"""
Created on Tue Feb 10 12:15:06 2015

@author: noore

Description:
    Generate a toy reaction network model with N intermediate metabolites
    and N+1 reactions.

"""

import numpy as np

from ecm.cost_function import EnzymeCostFunction


N = 2

S = np.zeros((N + 2, N + 1))
for i in range(N + 1):
    S[i, i] = -1.0
    S[i + 1, i] = 1.0
v = np.ones((N + 1, 1))
kcat = np.ones((N + 1, 1)) * 100
dG0 = -5.0 * np.ones((N + 1, 1))
KMM = np.ones(S.shape)
KMM[S < 0] = 3e-2
KMM[S > 0] = 1e-2

lnC_bounds = np.ones((S.shape[0], 2)) * np.log(1e-4)
lnC_bounds[1:-1, 0] = np.log(1e-6)
lnC_bounds[1:-1, 1] = np.log(1e-2)

ecf1 = EnzymeCostFunction(
    S, v, kcat, dG0, KMM=KMM, lnC_bounds=lnC_bounds, params={"version": 1}
)

mdf1_sol, lnC0 = ecf1.MDF()
print("MDF: ", mdf1_sol)
print("ECM1: ", ecf1.ECM(lnC0).T)

ecf2 = EnzymeCostFunction(
    S, v, kcat, dG0, KMM=KMM, lnC_bounds=lnC_bounds, params={"version": 2}
)
mdf2_sol, lnC0 = ecf2.MDF()
print("MDF: ", mdf2_sol)
print("ECM2: ", ecf2.ECM(lnC0).T)
