import numpy as np
import pandas as pd
import pytest
from equilibrator_api import Q_
from equilibrator_api.bounds import Bounds
from path import Path
from sbtab import SBtab

from ecm.cost_function import EnzymeCostFunction
from ecm.model import ECMmodel
from ecm.util import RT


@pytest.fixture(scope="module")
def toy_ecf() -> EnzymeCostFunction:
    Nr = 3
    Nc = 4
    cids = ["C%04d" % i for i in range(Nc)]

    S = np.zeros((Nc, Nr))

    S[0, 0] = -1
    S[1, 0] = 1
    S[2, 0] = 1
    S[1, 1] = -1
    S[2, 1] = 1
    S[2, 2] = -1
    S[3, 2] = 1

    v = np.array([1.0, 1.0, 2.0], ndmin=2).T
    kcat = np.array([1.0, 1.0, 1.0], ndmin=2).T
    dGm_r = np.array([-3.0, -2.0, -3.0], ndmin=2).T
    dG0_r = dGm_r - S.T @ np.ones((Nc, 1)) * RT.to("kJ/mol").magnitude * np.log(
        1e-3
    )
    K_M = np.ones(S.shape)
    K_M[S < 0] = 0.09
    K_M[S > 0] = 0.01

    lnC_bounds = np.log(np.array([[1e-9] * Nc, [1e-1] * Nc], ndmin=2).T)
    bounds = Bounds(default_lb=Q_("1 nM"), default_ub=Q_("0.1 M"))

    A_act = np.zeros(S.shape)
    A_inh = np.zeros(S.shape)
    K_act = np.ones(S.shape)
    K_inh = np.ones(S.shape)

    return EnzymeCostFunction(
        S,
        v,
        kcat,
        dG0_r,
        K_M,
        lnC_bounds,
        None,
        None,
        A_act,
        A_inh,
        K_act,
        K_inh,
    )


def test_toy_ecm(toy_ecf):
    np.random.seed(2013)

    mdf, lnC0 = toy_ecf.MDF()
    assert mdf == pytest.approx(22.72 / RT.to("kJ/mol").magnitude, rel=0.1)

    lnC = toy_ecf.ECM(n_iter=5)
    concs = np.exp(lnC)
    costs = toy_ecf.ECF(lnC)

    assert concs[0, 0] == pytest.approx(0.1, rel=0.1)
    assert concs[1, 0] == pytest.approx(0.015, rel=0.1)
    assert concs[2, 0] == pytest.approx(0.0067, rel=0.1)
    assert concs[3, 0] == pytest.approx(1e-9, rel=1)
    assert costs[0, 0] == pytest.approx(3.93, rel=0.1)
    assert costs[1, 0] == pytest.approx(3.08, rel=0.1)
    assert costs[2, 0] == pytest.approx(5.22, rel=0.1)


def test_ecm(toy_ecf):
    np.random.seed(2013)

    test_dir = Path(__file__).abspath().parent

    df_dict = {
        name: pd.read_csv(str(test_dir / f"test_{name}.csv"))
        for name in ECMmodel.DATAFRAME_NAMES
    }

    model = ECMmodel(
        df_dict, ecf_params={"bound_unit": "M", "flux_unit": "mM/s"}
    )

    lnC_MDF = model.MDF()
    lnC_ECM = model.ECM(lnC0=lnC_MDF, n_iter=5)

    score_MDF = model.ecf.ECF(lnC_MDF).sum()
    score_ECM = model.ecf.ECF(lnC_ECM).sum()

    assert score_MDF == pytest.approx(0.077, rel=0.1)
    assert score_ECM == pytest.approx(0.015, rel=0.1)


def test_sbtab(toy_ecf):
    test_dir = Path(__file__).abspath().parent
    modeldata_fname = str(
        test_dir / "example_pathway_central_metabolism_pH7.00_I0.10_ECM.tsv"
    )

    sbtabdoc = SBtab.read_csv(modeldata_fname, "model")
    ecf_params = {"regularization": "volume"}

    model = ECMmodel.FromSBtab(sbtabdoc, ecf_params=ecf_params)
    lnC_MDF = model.MDF()
    lnC_ECM = model.ECM(lnC0=lnC_MDF, n_iter=5)

    assert lnC_MDF[9, 0] == pytest.approx(-5.9, rel=0.1)
    assert lnC_ECM[9, 0] == pytest.approx(-8.9, rel=0.1)

    score_MDF = model.ecf.ECF(lnC_MDF).sum()
    score_ECM = model.ecf.ECF(lnC_ECM).sum()

    assert score_MDF == pytest.approx(3.3e-3, rel=0.1)
    assert score_ECM == pytest.approx(5.6e-4, rel=0.1)
